use <MCAD/boxes.scad>
use <radiusedCylinder.scad>
$fa=1;
$fs=0.4;

height_upper = 4;
height_lower = 4;

num_hole_rows = 1;
hole_num = 6;
//hole_offset = 25;
//hole_length = 24.5;
hole_length = 63.5 * 0.5 - height_upper * 1.5;
//hole_length_inner = 18;
hole_length_inner = hole_length;
hole_width = 4;
hole_width_inner = 7.5;
hole_width_inner_slit = 3;
hole_inner_bump_extrude = 0.75;
hole_width_end = 10;
hole_spacing = height_upper * 0.5;

mounting_hole_inner_width = 5;
mounting_hole_outer_width = 11;
mounting_hole_lower_height = 3;
mounting_hole_ovality = 1.5;

length_upper = hole_num * (hole_length + height_upper) + (hole_num - 1) * hole_spacing + height_upper * 1.5;

length_lower = length_upper - height_upper * 2 - hole_width_end * 2 - height_lower * 2;
width_lower = mounting_hole_outer_width + height_upper;

hole_length_end = width_lower;

hole_offset_inner = width_lower * 0.5 + height_lower + hole_width_inner * 0.5;
width_upper = (hole_offset_inner - height_upper * 0.5 + num_hole_rows * (hole_width_inner + height_upper) + (hole_width + height_upper)) * 2;

upper_dim = [length_upper, width_upper, height_upper];
lower_dim = [length_lower, width_lower, height_lower];

module create_lower(dim)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  //difference()
  {
    hull()
    {
      roundedBox(
        size=[ length, width, height ],
        radius = height * 0.5
      );
      
      translate([ 0, 0, height ])
      {
        roundedBox(
          size=[ length + height * 2, width + height * 2, height ],
          radius = height * 0.5
        );
      }
    }
//    translate([ 0, 0, height * 2 + 1 ])
//    {
//      roundedBox(
//        size=[ length + height * 2, width + height * 2, height * 3 ],
//        radius = 0
//      );
//    }
  }
}

module create_upper_edge(height, radius, quadrant)
{
  rotate_extrude(angle = 360)
  {
    translate([radius, 0])
        circle(height * 0.5);
  }
}

module create_upper(dim)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  radius = 4;
  dx = length * 0.5 - radius - height * 0.5;
  dy = width * 0.5 - radius - height * 0.5;
  
  hull()
  {
    for (x = [ -1, 1 ])
    {
      for (y = [ -1, 1 ])
      translate([ x * dx, y * dy, 0])
      {
        create_upper_edge(height, radius, 0);
      }
    }
  }
}

module create_outer_holes(dim, height_offset)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  for (dx=[0 : 1 : hole_num - 1])
  {
    offset = (dx - ((hole_num - 1) * 0.5)) * (hole_length + hole_spacing + height);
    dyy = (width - hole_width) * 0.5 - height;
    for (dy = [ -dyy, dyy ])
    {
      translate([ offset, dy, height_offset ])
      {
        radiusedCylinder(
          hole_width * 0.5,
          height,
          height * 0.5,
          height * 0.5,
          [[ hole_length * -0.5 + hole_width * 0.5, 0 ], [ hole_length * 0.5 - hole_width * 0.5, 0 ]]
        );
      }
    }
  }
}

module create_inner_holes(dim, height_offset, row)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  bump_radius = height * 0.5;
  
  for (dx=[0 : 1 : hole_num - 1])
  {
    offset = (dx - ((hole_num - 1) * 0.5)) * (hole_length + hole_spacing + height);
    dyy = hole_offset_inner + row * (hole_width_inner + height);
    for (dy = [ -dyy, dyy ])
    {
      translate([ offset, dy, height_offset ])
      {
        difference()
        {
          radiusedCylinder(
            hole_width_inner_slit * 0.5,
            height,
            height * 0.5,
            height * 0.5,
            [[ hole_length_inner * -0.5 + hole_width_inner_slit * 0.5, 0 ], [ hole_length_inner * 0.5 - hole_width_inner_slit * 0.5, 0 ]]
          );
          
          for (sdx = [ -1, 1 ])
            for (sdy = [ -1, 1 ])
              translate([ sdx * (hole_width_inner * 0.5 + bump_radius * 0.5), sdy * (hole_width_inner_slit * 0.5 + bump_radius - hole_inner_bump_extrude), height * 0.5 ])
                sphere(bump_radius);
        }
        radiusedCylinder(
          hole_width_inner * 0.5,
          height,
          height * 0.5,
          height * 0.5,
          [[ 0, 0 ], [ 0, 0 ]]
        );
      }
    }
  }
}

module create_end_holes(dim, height_offset)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  offset = length * 0.5 - hole_width_end * 0.5 - height;
  
  for (dx=[ -offset, offset ])
  {
    translate([ dx, 0, height_offset ])
    {
      radiusedCylinder(
        hole_width_end * 0.5,
        height,
        height * 0.5,
        height * 0.5,
        [[ 0, hole_length_end * -0.5 + hole_width_end * 0.5 ], [ 0, hole_length_end * 0.5 - hole_width_end * 0.5 ]]
      );
    }
  }
}

module create_fins(dim, height_offset)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  w = width * 0.5 - height * 0.5;
  w2 = mounting_hole_outer_width * 0.5 + height * 0.5;
  fin_height = [
    [ 0,   0 ],
    [ 0.09, 1.5 ],
    [ 0.19, 2.25 ],
    [ 0.4, 3.25  ],
    [ 0.6, 4 ],
    [ 0.8, 4.5 ],
    [ 0.8,   0 ],
  ];


  for (dx = [ 0 : 1 : hole_num ])
  {
    dxx = (dx - ((hole_num) * 0.5)) * 63.5 * 0.5;
    for (dy = [ -1, 1 ])
    {
      hull()
      {
        for (dz = [ 0 : 1 : len(fin_height) - 1 ])
        {
          dzz = fin_height[dz][1];
          r = fin_height[dz][0];
          translate([ dxx, dy * (w2 * (1 - r) + w * r), height_offset + height * 0.5 + dzz ])
          {
            sphere(height * 0.5);
          }
        }
      }
    }
  }
}

module create_lower_fins(dim, height_offset)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  w = width * 0.5 - height * 0.5;
  
  for (dx = [ 0 : 1 : hole_num - 2 ])
  {
    dxx = (dx - ((hole_num - 2) * 0.5)) * 63.5 * 0.5;
    hull()
    {
      for (dy = [ -1, 1 ])
      {
        translate([ dxx, dy * w, height_offset + height * 0.5 ])
        {
          sphere(height * 0.5);
        }
        translate([ dxx, dy * (width_lower * 0.5 - height * 0.5), height * 0.5 ])
        {
          sphere(height * 0.5);
        }
      }
    }
  }
}

module create_single_mounting_hole(height, ovality = 0)
{
  hull()
  {
    for (dx = [ -ovality * 0.5, ovality * 0.5 ])
    translate([ dx, 0, -1 ])
    {
      cylinder(height + 2, mounting_hole_inner_width * 0.5, mounting_hole_inner_width * 0.5);
    }
  }
  translate([ 0, 0, mounting_hole_lower_height ])
  {
    radiusedCylinder(
      mounting_hole_outer_width * 0.5,
      height - mounting_hole_lower_height,
      0,
      height_upper * 0.5,
      [[ -ovality, 0 ], [ ovality, 0 ]]
    );
  }
}

module create_mounting_holes(dim, height_offset)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  for (dx = [ 0 : 1 : hole_num - 2 ])
  {
    dxx = (dx - ((hole_num - 2) * 0.5)) * 63.5 * 0.5;
    translate([ dxx, 0, 0 ])
    {
      create_single_mounting_hole(height + height_offset, mounting_hole_ovality);
    }
  }
}

module create_extra_mounting_holes(dim, height_offset)
{
  length = dim[0];
  width = dim[1];
  height = dim[2];
  
  for (dx = [ 0 : 1 : hole_num - 3 ])
  {
    dxx = (dx - ((hole_num - 3) * 0.5)) * 63.5 * 0.5;
    translate([ dxx, 0, 0 ])
    {
      create_single_mounting_hole(height + height_offset);
    }
  }
}

module create_body(upper_dim, lower_dim)
{
  union()
  {
    difference()
    {
      union()
      {
        translate([ 0, 0, lower_dim[2] + upper_dim[2] * 0.5 ])
        {
          create_upper(upper_dim);
        }
        translate([ 0, 0, lower_dim[2] * 0.5 ])
        {
          create_lower(lower_dim);
        }
      }
      create_outer_holes(upper_dim, height_lower);
      for (i = [ 0 : 1 : num_hole_rows - 1])
        create_inner_holes(upper_dim, height_lower, i);
      create_end_holes(upper_dim, height_lower);
    }
    create_lower_fins(upper_dim, height_lower);
//    create_end_fins(upper_dim, height_lower);
  }
}

module create_thing()
{
  difference()
  {
    create_body(upper_dim, lower_dim);
    create_mounting_holes(upper_dim, height_lower);
    
//    cube([50, 50, 50]);
//    create_extra_mounting_holes(upper_dim, height_lower);
  }
  create_fins(upper_dim, height_lower);
}

module create_sample_payload()
{
  v = [
    [ 60, 0, "red" ],
    [ 65, 0.2, "green" ],
    [ 75, 0.4, "blue" ],
    [ 95, 0.7, "white" ],
    [ 150, 1.5, "brown" ],
  ];
  for (c = v)
  {
    d = c[0];
    h = c[1];
    colo = c[2];
    r = d * 0.5;
    color(colo)
    translate([ r, 0, r + height_lower + height_upper + h ])
      rotate( [ 0, 90, 0 ] )
        cylinder(1, r, r);
  }
}

create_thing();
//create_sample_payload();