/*
 radiusedCylinder - creates a cylinder with round chamfered ends.
 Written by 'enif', a user on the RepRap Forum (in the OpenSCAD topic).
 Edited, reformatted, renamed and generally mucked around by David Jenkins. 
 (C) 2019

 This work is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; version 2.

 This work is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE. See the version 2 of the GNU General Public License for
 more details.
*/
/* 
Usage notes
===========

The module radiusedCylinder() takes as parameters the radius r and height h of the cylinder 
and in addition the chamfer radii b for bottom and t for top, where a positive chamfer radius 
goes to the outside and a negative to the inside of the main cylinder. The slices parameter 
gives the number of slices for rounding the chamfer. The optional offset array can be used 
to generate a convex hull at the slice level (this cannot be done at the object level because 
the final object is not convex if outside chamfering is used). 
In the example code the offsets are used to make a slot instead of a round hole. Finally, 
the parameter eps can be used to add a tiny overlap of the slices.
*/

module radiusedCylinder(
   r,              // cylinder radius
   h,              // cylinder height
   b=0,            // bottom chamfer radius (=0 none, >0 outside, <0 inside)
   t=0,            // top chamfer radius (=0 none, >0 outside, <0 inside)
   offset=[[0,0]], // optional offsets in X and Y to create convex hulls at slice level
   slices=10,      // number of slices used for chamfering
   eps=0.01,       // tiny overlap of slices
){
    astep=90/slices;
    hull()
        for(o = offset)
            translate([o[0],o[1],abs(b)-eps])
                cylinder(r=r,h=h-abs(b)-abs(t)+2*eps);
    if(b)
        for(a=[0:astep:89.999])
            hull()
                for(o = offset)
                    translate([o[0],o[1],abs(b)-abs(b)*sin(a+astep)-eps])
                        cylinder(r2=r+(1-cos(a))*b,r1=r+(1-cos(a+astep))*b,h=(sin(a+astep)-sin(a))*abs(b)+2*eps);
    if(t)
        for(a=[0:astep:89.999])
            hull()
                for(o = offset)
                    translate([o[0],o[1],h-abs(t)+abs(t)*sin(a)-eps])
                        cylinder(r1=r+(1-cos(a))*t,r2=r+(1-cos(a+astep))*t,h=(sin(a+astep)-sin(a))*abs(t)+2*eps);
}

// Example (uncomment the following 3 lines to run)
//difference(){
//   translate([-12.5,-12.5,0])cube(25);
//   radiusedCylinder(3,25,3,3,[[-2,0],[2,0]]);
//}